 package com.example.parktest.data;


import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class SaveData {


    Context ctx;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    public SaveData(Context ctx1){
        this.ctx = ctx1;
        preferences = PreferenceManager.getDefaultSharedPreferences(ctx);
        editor = preferences.edit();
    }


    public void saveLongitude(String longitude) {
        editor.putString("longitude", longitude);
        editor.commit();
    }

    public String getLongitude(){
        return preferences.getString("longitude", "");

    }


    public void saveLattitude(String lattitude) {
        editor.putString("lattitude", lattitude);
        editor.commit();
    }

    public String getLattitude(){
        return preferences.getString("lattitude", "");

    }


}
