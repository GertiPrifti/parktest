package com.example.parktest.firebase;


import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;


public class MyFirebaseInstanceIDService   extends FirebaseInstanceIdService {


    @Override
    public void onTokenRefresh() {

        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d("myfirebase", "New Token: " + refreshedToken);


        System.out.print("printotokenin"+refreshedToken);
        //You can save the token into third party server to do anything you want
    }
}
