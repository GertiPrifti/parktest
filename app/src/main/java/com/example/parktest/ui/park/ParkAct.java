package com.example.parktest.ui.park;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import com.example.parktest.R;
import com.example.parktest.data.SaveData;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;


public class ParkAct extends AppCompatActivity    {

    RadioGroup radioGroupTimes;
    RadioGroup radioGroupMoney;

    RadioButton radioButtonMoney;
    RadioButton radioButtonTime ;

    Button buttonPark;
    TextView textViewPrice;

    String value;

    DatabaseReference myRef;
    FirebaseDatabase database;

    SaveData saveData;

    EditText editTextTarg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_park);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        saveData = new SaveData(getApplicationContext());


        database  = FirebaseDatabase.getInstance();
        myRef = database.getReference("data");

        radioGroupMoney = (RadioGroup)findViewById(R.id.radioGroupMoney);
        radioGroupTimes = (RadioGroup)findViewById(R.id.radioGroupTime);
        textViewPrice = (TextView)findViewById(R.id.textViewPrice);
        editTextTarg = (EditText)findViewById(R.id.editTextTarg);
        value = "100 Leke";

        buttonPark = (Button)findViewById(R.id.buttonPark);

        textViewPrice.setText("100 Leke");

        radioGroupMoney.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                // This will get the radiobutton that has changed in its check state
                RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
                // This puts the value (true/false) into the variable
                boolean isChecked = checkedRadioButton.isChecked();

                int selectedIdMoney = radioGroupMoney.getCheckedRadioButtonId();
                int radioGroupTime = radioGroupTimes.getCheckedRadioButtonId();


                radioButtonMoney = (RadioButton) findViewById(selectedIdMoney);
                radioButtonTime = (RadioButton) findViewById(radioGroupTime);

                Toast.makeText(getApplicationContext(), radioButtonMoney.getText().toString()+" "+radioButtonTime.getText().toString(), Toast.LENGTH_SHORT).show();
                // If the radiobutton that has changed in check state is now checked...

                setValue(radioButtonMoney.getText().toString(),radioButtonTime.getText().toString());

            }
        });

        radioGroupTimes.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                // This will get the radiobutton that has changed in its check state
                RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
                // This puts the value (true/false) into the variable
                boolean isChecked = checkedRadioButton.isChecked();

                int selectedIdMoney = radioGroupMoney.getCheckedRadioButtonId();
                int radioGroupTime = radioGroupTimes.getCheckedRadioButtonId();


                radioButtonMoney = (RadioButton) findViewById(selectedIdMoney);
                radioButtonTime = (RadioButton) findViewById(radioGroupTime);

                Toast.makeText(getApplicationContext(), radioButtonMoney.getText().toString()+" "+radioButtonTime.getText().toString(), Toast.LENGTH_SHORT).show();
                // If the radiobutton that has changed in check state is now checked...


                setValue(radioButtonMoney.getText().toString(),radioButtonTime.getText().toString());
            }
        });

        buttonPark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int selectedIdMoney = radioGroupMoney.getCheckedRadioButtonId();
                int radioGroupTime = radioGroupTimes.getCheckedRadioButtonId();


                radioButtonMoney = (RadioButton) findViewById(selectedIdMoney);
                radioButtonTime = (RadioButton) findViewById(radioGroupTime);

                Toast.makeText(getApplicationContext(), radioButtonMoney.getText().toString()+" "+radioButtonTime.getText().toString(), Toast.LENGTH_SHORT).show();

                sendData();
            }
        });

    }

    private void setValue(String time,String money){

        String timeSring = time.substring (0,3);
        String shshhs  = timeSring.replaceAll("\\s", "");

        String sd2 = money.substring (0,1);

        int result = Integer.parseInt(shshhs);
        int resulxt = Integer.parseInt(sd2);

        value = String.valueOf(result*resulxt);

        textViewPrice.setText(String.valueOf(result*resulxt)+" Leke");

    }

    private  void sendData(){

        myRef.child("targ").setValue(editTextTarg.getText().toString());
        myRef.child("value").setValue(value);

        myRef.child("lattitude").setValue(saveData.getLattitude());
        myRef.child("longitude").setValue(saveData.getLongitude());
    }


}
